%define _lto_cflags %{nil}

Name:           cryptopp
Version:        8.9.0
Release:        2
Summary:        C++ class library of cryptographic schemes
License:        Boost
URL:            http://www.cryptopp.com/
Source0:        http://www.cryptopp.com/cryptopp890.zip
Source1:        cryptopp.pc
#Patch0:         https://github.com/weidai11/cryptopp/commit/94aba0105efa.patch
Patch0:         backport-chore-fix-typos.patch
Patch1:         backport-Clear-GCC-overflow-warning.patch

BuildRequires:  doxygen
BuildRequires:  clang
# %ifarch i686 x86_64
# BuildRequires:  nasm
# %endif
BuildRequires:  make

# Obsoletes pycryptopp to avoid breaking upgrades
Obsoletes:  pycryptopp < 0.7
Provides:   pycryptopp = 0.7


%description
Crypto++ Library is a free C++ class library of cryptographic schemes.
See http://www.cryptopp.com/ for a list of supported algorithms.

One purpose of Crypto++ is to act as a repository of public domain
(not copyrighted) source code. Although the library is copyrighted as a
compilation, the individual files in it are in the public domain.

%package devel
Summary:        Header files and development documentation for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Crypto++ Library is a free C++ class library of cryptographic schemes.

This package contains the header files and development documentation
for %{name}.

%package doc
Summary:        Documentation for %{name}
BuildArch:      noarch

%description doc
Crypto++ Library is a free C++ class library of cryptographic schemes.

This package contains documentation for %{name}.

%package progs
Summary:        Programs for manipulating %{name} routines
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description progs
Crypto++ Library is a free C++ class library of cryptographic schemes.

This package contains programs for manipulating %{name} routines.

%prep
%autosetup -c -p1
perl -pi -e 's/\r$//g' License.txt Readme.txt


%build
%{set_build_flags}
%make_build -f GNUmakefile \
  ZOPT='' \
  shared cryptest.exe

doxygen

%install
%make_install INSTALL="install -p -c " PREFIX="%{_prefix}" LIBDIR="%{_libdir}"

# Install the pkg-config file
install -D -m644 %{SOURCE1} $RPM_BUILD_ROOT%{_libdir}/pkgconfig/cryptopp.pc
# Fill in the variables
sed -i "s|@PREFIX@|%{_prefix}|g" $RPM_BUILD_ROOT%{_libdir}/pkgconfig/cryptopp.pc
sed -i "s|@LIBDIR@|%{_libdir}|g" $RPM_BUILD_ROOT%{_libdir}/pkgconfig/cryptopp.pc
sed -i "s|@VERSION@|%{version}}|g" $RPM_BUILD_ROOT%{_libdir}/pkgconfig/cryptopp.pc

mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/TestVectors
mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/TestData
install -m644 TestVectors/* $RPM_BUILD_ROOT%{_datadir}/%{name}/TestVectors
install -m644 TestData/* $RPM_BUILD_ROOT%{_datadir}/%{name}/TestData

# Rename cryptest
mv $RPM_BUILD_ROOT%{_bindir}/cryptest.exe \
   $RPM_BUILD_ROOT%{_bindir}/cryptest

# Remove static lib
rm  %{buildroot}%{_libdir}/libcryptopp.a

%check
./cryptest.exe v

%ldconfig_scriptlets

%files
%{_libdir}/libcryptopp.so.8*
%doc Readme.txt
%license License.txt

%files devel
%{_includedir}/cryptopp
%{_libdir}/libcryptopp.so
%{_libdir}/pkgconfig/cryptopp.pc

%files doc
%license License.txt
%doc html-docs/*

%files progs
%{_bindir}/cryptest
%{_datadir}/%{name}

%changelog
* Tue Jul 16 2024 zhangxingrong-<zhangxingrong@uniontech.cn> - 8.9.0-2
- chore: fix typos (#1274)
- Clear GCC overflow warning

* Tue Mar 05  mengchaoming <mengchaoming@kylinos.cn> - 8.9.0-1
- upgrade to version 8.9.0

* Tue Aug 22 2023 leeffo <liweiganga@uniontech.com> - 8.8.0-1
- upgrade to version 8.8.0

* Tue Feb 28 2023 lilong <lilong@kylinos.cn> - 8.7.0-1
- Upgrade to 8.7.0

* Fri Jul 29 2022 liweiganga <liweiganga@uniontech.com> - 8.6.0-1
- update to 8.6.0

* Thu Sep 10 2020 liurenxu <liurenxu@talkweb.com.cn> - 8.2.0-1
- Package init
